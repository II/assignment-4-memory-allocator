#include <assert.h>
#include <stdio.h>
#include <unistd.h>

#include "mem.h"
#include "mem_internals.h"

#define MIN_BLOCK_SIZE 24
#define TEST_HEAP_SIZE 4096

static struct block_header* header_from_ptr(void* ptr) {
    return (struct block_header*)((uint8_t*)ptr - offsetof(struct block_header, contents));
}

static void test_successful_memory_allocation() {
    printf("Testing successful memory allocation...\n");

    void* heap = heap_init(TEST_HEAP_SIZE);
    assert(heap);

    void* allocated_block = _malloc(100);
    assert(allocated_block);

    _free(allocated_block);
    heap_term();
    printf("Successful memory allocation test passed\n\n");
}

static void test_release_one_of_several_blocks() {
    printf("Testing release of one block out of several...\n");

    void* heap = heap_init(TEST_HEAP_SIZE);
    assert(heap);

    void* block1 = _malloc(100);
    void* block2 = _malloc(200);

    _free(block2);

    struct block_header* header1 = header_from_ptr(block1);
    struct block_header* header2 = header_from_ptr(block2);

    assert(header1->next == header2);
    assert(header2->is_free);

    _free(block1);
    heap_term();
    printf("Release of one block out of several test passed\n\n");
}

static void test_release_two_of_several_blocks() {
    printf("Testing release of two blocks out of several...\n");

    void* heap = heap_init(TEST_HEAP_SIZE);
    assert(heap);

    void* block1 = _malloc(100);
    void* block2 = _malloc(200);
    void* block3 = _malloc(300);

    _free(block1);
    _free(block3);

    struct block_header* header1 = header_from_ptr(block1);
    struct block_header* header2 = header_from_ptr(block2);
    struct block_header* header3 = header_from_ptr(block3);

    assert(header1->is_free);
    assert(!(header2->is_free));
    assert(header3->is_free);

    _free(block2);
    heap_term();
    printf("Release of two blocks out of several test passed\n\n");
}

static void test_expand_existing_region() {
    printf("Testing expansion of existing memory region...\n");

    void* heap = heap_init(TEST_HEAP_SIZE);
    assert(heap);

    size_t initial_region_size = ((struct region*)heap)->size;

    void* block = _malloc(5 * TEST_HEAP_SIZE);
    struct block_header* new_block_header = header_from_ptr(block);
    size_t expanded_region_size = ((struct region*)heap)->size;

    assert(initial_region_size < expanded_region_size);
    assert(new_block_header);
    assert(new_block_header->capacity.bytes >= 5 * TEST_HEAP_SIZE);

    _free(block);
    heap_term();
    printf("Expansion of existing memory region test passed\n\n");
}

static void test_allocate_new_region() {
    printf("Testing allocation in a new memory region...\n");

    void* heap = heap_init(TEST_HEAP_SIZE);
    assert(heap);

    void* hole = mmap(HEAP_START + REGION_MIN_SIZE, 100, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    assert(hole == HEAP_START + REGION_MIN_SIZE);

    void* new_block = _malloc(REGION_MIN_SIZE - offsetof(struct block_header, contents));
    struct block_header* new_block_header = header_from_ptr(new_block);

    assert(new_block);
    assert(new_block != hole);
    assert(new_block_header->capacity.bytes >= (REGION_MIN_SIZE - offsetof(struct block_header, contents)));

    munmap(hole, 100);
    _free(new_block);
    heap_term();
    printf("Allocation in a new memory region test passed\n\n");
}

int main() {
    test_successful_memory_allocation();
    test_release_one_of_several_blocks();
    test_release_two_of_several_blocks();
    test_expand_existing_region();
    test_allocate_new_region();
    return 0;
}
